# RÉSO
## 1. Affichage d'informations sur la pile TCP/IP locale

🌞 Affichez les infos des cartes réseau de votre PC

```
PS C:\Users\Usager\reso> ipconfig /all

Carte Ethernet Ethernet :

   Statut du média. . . . . . . . . . . . : Média déconnecté
   Suffixe DNS propre à la connexion. . . :
   Description. . . . . . . . . . . . . . : Intel(R) Ethernet Connection (4) I219-V
   Adresse physique . . . . . . . . . . . : 8C-16-45-D4-A8-45
   DHCP activé. . . . . . . . . . . . . . : Non
   Configuration automatique activée. . . : Oui

Carte réseau sans fil Wi-Fi :

   Suffixe DNS propre à la connexion. . . :
   Description. . . . . . . . . . . . . . : Intel(R) Dual Band Wireless-AC 8265
   Adresse physique . . . . . . . . . . . : 20-16-B9-29-84-75
   DHCP activé. . . . . . . . . . . . . . : Oui
   Configuration automatique activée. . . : Oui
   Adresse IPv6 de liaison locale. . . . .: fe80::b418:90a8:d4c4:3f44%35(préféré)
   Adresse IPv4. . . . . . . . . . . . . .: 10.33.48.12(préféré)
   Masque de sous-réseau. . . . . . . . . : 255.255.252.0
   Bail obtenu. . . . . . . . . . . . . . : 11 octobre 2023 03:00:58
   Bail expirant. . . . . . . . . . . . . : 12 octobre 2023 03:00:54
   Passerelle par défaut. . . . . . . . . : 10.33.51.254
   Serveur DHCP . . . . . . . . . . . . . : 10.33.51.254
   IAID DHCPv6 . . . . . . . . . . . : 169875129
   DUID de client DHCPv6. . . . . . . . : 00-01-00-01-2A-D2-4C-9E-8C-16-45-D4-A8-45
   Serveurs DNS. . .  . . . . . . . . . . : 10.33.10.2
                                       8.8.8.8
   NetBIOS sur Tcpip. . . . . . . . . . . : Activé
```
- nom, adresse MAC et adresse IP de l'interface WiFi
 Mac :   Adresse physique
 adresse IP : Adresse IPv4

- nom, adresse MAC et adresse IP de l'interface Ethernet
 MAC :  Adresse physique .
 adresse IP : Aucune


🌞 Affichez votre gateway

```
PS C:\Users\Usager\reso> ipconfig /all

Carte réseau sans fil Wi-Fi :

   Suffixe DNS propre à la connexion. . . :
   Description. . . . . . . . . . . . . . : Intel(R) Dual Band Wireless-AC 8265
   Adresse physique . . . . . . . . . . . : 20-16-B9-29-84-75
   DHCP activé. . . . . . . . . . . . . . : Oui
   Configuration automatique activée. . . : Oui
   Adresse IPv6 de liaison locale. . . . .: fe80::b418:90a8:d4c4:3f44%35(préféré)
   Adresse IPv4. . . . . . . . . . . . . .: 10.33.48.12(préféré)
   Masque de sous-réseau. . . . . . . . . : 255.255.252.0
   Bail obtenu. . . . . . . . . . . . . . : 11 octobre 2023 03:00:58
   Bail expirant. . . . . . . . . . . . . : 12 octobre 2023 03:00:54
   Passerelle par défaut. . . . . . . . . : 10.33.51.254
   Serveur DHCP . . . . . . . . . . . . . : 10.33.51.254
   IAID DHCPv6 . . . . . . . . . . . : 169875129
   DUID de client DHCPv6. . . . . . . . : 00-01-00-01-2A-D2-4C-9E-8C-16-45-D4-A8-45
   Serveurs DNS. . .  . . . . . . . . . . : 10.33.10.2
                                       8.8.8.8
   NetBIOS sur Tcpip. . . . . . . . . . . : Activé
``` 
- adresse IP de la passerelle :  10.33.51.254


🌞 Déterminer la MAC de la passerelle

```
PS C:\Users\Usager\reso>   arp -a

Interface : 10.33.48.12 --- 0x23
  Adresse Internet      Adresse physique      Type
  10.33.51.254          7c-5a-1c-cb-fd-a4     dynamique
  10.33.51.255          ff-ff-ff-ff-ff-ff     statique
  224.0.0.22            01-00-5e-00-00-16     statique
  224.0.0.251           01-00-5e-00-00-fb     statique
  224.0.0.252           01-00-5e-00-00-fc     statique
  239.255.255.250       01-00-5e-7f-ff-fa     statique
  255.255.255.255       ff-ff-ff-ff-ff-ff     statique
```
- passerelle MAC : 10.33.51.254

## 2. Modifications des informations

### A. Modification d'adresse IP (part 1)

🌞 Utilisez l'interface graphique de votre OS pour changer d'adresse IP :
``` 
PS C:\Users\Usager\reso> ipconfig /all
 Carte réseau sans fil Wi-Fi :

   Suffixe DNS propre à la connexion. . . :
   Description. . . . . . . . . . . . . . : Intel(R) Dual Band Wireless-AC 8265
   Adresse physique . . . . . . . . . . . : 20-16-B9-29-84-75
   DHCP activé. . . . . . . . . . . . . . : Non
   Configuration automatique activée. . . : Oui
   Adresse IPv6 de liaison locale. . . . .: fe80::b418:90a8:d4c4:3f44%35(préféré)
   Adresse IPv4. . . . . . . . . . . . . .: 10.33.48.18(préféré)
   Masque de sous-réseau. . . . . . . . . : 255.255.252.0
   Passerelle par défaut. . . . . . . . . : 10.33.51.254
   IAID DHCPv6 . . . . . . . . . . . : 169875129
   DUID de client DHCPv6. . . . . . . . : 00-01-00-01-2A-D2-4C-9E-8C-16-45-D4-A8-45
   Serveurs DNS. . .  . . . . . . . . . . : fec0:0:0:ffff::1%1
                                       fec0:0:0:ffff::2%1
                                       fec0:0:0:ffff::3%1
   NetBIOS sur Tcpip. . . . . . . . . . . : Activé

```
## II. Exploration locale en duo
### 1. Prérequis
### 2. Câblage
#### Création du réseau (oupa)
### 3. Modification d'adresse IP

🌞 Modifiez l'IP des deux machines pour qu'elles soient dans le même réseau
```
PS C:\Users\Usager> ipconfig /all

Carte Ethernet Ethernet 2 :

   Suffixe DNS propre à la connexion. . . :
   Description. . . . . . . . . . . . . . : VirtualBox Host-Only Ethernet Adapter
   Adresse physique . . . . . . . . . . . : 0A-00-27-00-00-22
   DHCP activé. . . . . . . . . . . . . . : Non
   Configuration automatique activée. . . : Oui
   Adresse IPv6 de liaison locale. . . . .: fe80::2efc:5e24:97:8f92%34(préféré)
   Adresse IPv4. . . . . . . . . . . . . .: 192.168.56.1(préféré)
   Masque de sous-réseau. . . . . . . . . : 255.255.255.0
   Passerelle par défaut. . . . . . . . . :
   IAID DHCPv6 . . . . . . . . . . . : 1107951655
   DUID de client DHCPv6. . . . . . . . : 00-01-00-01-2A-D2-4C-9E-8C-16-45-D4-A8-45
   NetBIOS sur Tcpip. . . . . . . . . . . : Activé

Carte réseau sans fil Wi-Fi :

   Suffixe DNS propre à la connexion. . . : lan
   Adresse IPv6. . . . . . . . . . . . . .: 2001:861:81:9910:efd6:e0e7:6980:91a1
   Adresse IPv6 temporaire . . . . . . . .: 2001:861:81:9910:197a:b172:bb69:7379
   Adresse IPv6 de liaison locale. . . . .: fe80::b418:90a8:d4c4:3f44%33
   Adresse IPv4. . . . . . . . . . . . . .: 192.168.1.29
   Masque de sous-réseau. . . . . . . . . : 255.255.255.0
   Passerelle par défaut. . . . . . . . . : fe80::f24d:d4ff:fe26:295c%33

```
 - Adresse IPv4 : 192.168.56.1
Masque de sous-réseau : 255.255.255.0

Pour la carte réseau sans fil (Wi-Fi) :

- mon Adresse IPv4 : 192.168.1.29
Masque de sous-réseau : 255.255.255.0


🌞 Vérifier à l'aide d'une commande que votre IP a bien été changée

```
PS C:\Users\Usager> ipconfig 
Carte réseau sans fil Wi-Fi :

   Suffixe DNS propre à la connexion. . . : lan
   Adresse IPv6. . . . . . . . . . . . . .: 2001:861:81:9910:efd6:e0e7:6980:91a1
   Adresse IPv6 temporaire . . . . . . . .: 2001:861:81:9910:bd39:554:490f:36d7
   Adresse IPv6 de liaison locale. . . . .: fe80::b418:90a8:d4c4:3f44%33
   Adresse IPv4. . . . . . . . . . . . . .: 192.168.1.29
   Masque de sous-réseau. . . . . . . . . : 255.255.255.0
   Passerelle par défaut. . . . . . . . . : fe80::f24d:d4ff:fe26:295c%33
```

🌞 Vérifier que les deux machines se joignent

```
PS C:\Users\Usager> ping 192.168.56.1
Envoi d’une requête 'Ping'  192.168.56.1 avec 32 octets de données :
Réponse de 192.168.56.1 : octets=32 temps<1ms TTL=128
Réponse de 192.168.56.1 : octets=32 temps<1ms TTL=128
Réponse de 192.168.56.1 : octets=32 temps<1ms TTL=128
Réponse de 192.168.56.1 : octets=32 temps<1ms TTL=128

Statistiques Ping pour 192.168.56.1:
    Paquets : envoyés = 4, reçus = 4, perdus = 0 (perte 0%),
Durée approximative des boucles en millisecondes :
    Minimum = 0ms, Maximum = 0ms, Moyenne = 0ms
```
🌞 Déterminer l'adresse MAC de votre correspondant
```
PS C:\Users\Usager> arp -a 
Interface : 192.168.56.1 --- 0x22
  Adresse Internet      Adresse physique      Type
  192.168.56.255        ff-ff-ff-ff-ff-ff     statique
  224.0.0.2             01-00-5e-00-00-02     statique
  224.0.0.22            01-00-5e-00-00-16     statique
  224.0.0.251           01-00-5e-00-00-fb     statique
  224.0.0.252           01-00-5e-00-00-fc     statique
  224.0.2.3             01-00-5e-00-02-03     statique
  239.192.152.143       01-00-5e-40-98-8f     statique
  239.255.255.250       01-00-5e-7f-ff-fa     statique
```

### 4. Petit chat privé

🌞 sur le PC serveur avec par exemple l'IP 192.168.1.1

🌞 sur le PC client avec par exemple l'IP 192.168.1.2
🌞 Visualiser la connexion en cours
🌞 Pour aller un peu plus loin
### 5. Firewall
🌞 Activez et configurez votre firewall
### 6. Utilisation d'un des deux comme gateway
🌞Tester l'accès internet
🌞 Prouver que la connexion Internet passe bien par l'autre PC

## III. Manipulations d'autres outils/protocoles côté client
### 1. DHCP

🌞Exploration du DHCP, depuis votre PC
```
PS C:\Users\Usager> ipconfig /all
Carte réseau sans fil Wi-Fi :

   Suffixe DNS propre à la connexion. . . :
   Description. . . . . . . . . . . . . . : Intel(R) Dual Band Wireless-AC 8265
   Adresse physique . . . . . . . . . . . : 20-16-B9-29-84-75
   DHCP activé. . . . . . . . . . . . . . : Oui
   Configuration automatique activée. . . : Oui
   Adresse IPv6 de liaison locale. . . . .: fe80::b418:90a8:d4c4:3f44%33(préféré)
   Adresse IPv4. . . . . . . . . . . . . .: 10.33.48.12(préféré)
   Masque de sous-réseau. . . . . . . . . : 255.255.252.0
   Bail obtenu. . . . . . . . . . . . . . : 16 octobre 2023 03:17:25
   Bail expirant. . . . . . . . . . . . . : 17 octobre 2023 03:17:24
   Passerelle par défaut. . . . . . . . . : 10.33.51.254
   Serveur DHCP . . . . . . . . . . . . . : 10.33.51.254
   IAID DHCPv6 . . . . . . . . . . . : 169875129
   DUID de client DHCPv6. . . . . . . . : 00-01-00-01-2A-D2-4C-9E-8C-16-45-D4-A8-45
   Serveurs DNS. . .  . . . . . . . . . . : 10.33.10.2
                                       8.8.8.8
   NetBIOS sur Tcpip. . . . . . . . . . . : Activé
   ```
   - afficher l'adresse IP du serveur DHCP du réseau WiFi YNOV
   Serveur DHCP . . . . . . . . . . . . . : 10.33.51.254

   ```
   PS C:\Users\Usager> ipconfig /all
   Carte réseau sans fil Wi-Fi :

   Suffixe DNS propre à la connexion. . . :
   Description. . . . . . . . . . . . . . : Intel(R) Dual Band Wireless-AC 8265
   Adresse physique . . . . . . . . . . . : 20-16-B9-29-84-75
   DHCP activé. . . . . . . . . . . . . . : Oui
   Configuration automatique activée. . . : Oui
   Adresse IPv6 de liaison locale. . . . .: fe80::b418:90a8:d4c4:3f44%33(préféré)
   Adresse IPv4. . . . . . . . . . . . . .: 10.33.48.12(préféré)
   Masque de sous-réseau. . . . . . . . . : 255.255.252.0
   Bail obtenu. . . . . . . . . . . . . . : 16 octobre 2023 03:17:25
   Bail expirant. . . . . . . . . . . . . : 17 octobre 2023 03:17:24
   Passerelle par défaut. . . . . . . . . : 10.33.51.254
   Serveur DHCP . . . . . . . . . . . . . : 10.33.51.254
   IAID DHCPv6 . . . . . . . . . . . : 169875129
   DUID de client DHCPv6. . . . . . . . : 00-01-00-01-2A-D2-4C-9E-8C-16-45-D4-A8-45
   Serveurs DNS. . .  . . . . . . . . . . : 10.33.10.2
                                       8.8.8.8
   NetBIOS sur Tcpip. . . . . . . . . . . : Activé
   ```
- Date d'expiration du bail DHCP
Bail expirant. . . . . . . . . . . . . : 17 octobre 2023 03:17:24

### 2. DNS

🌞** Trouver l'adresse IP du serveur DNS que connaît votre ordinateur**

```
PS C:\Users\Usager> ipconfig /all
Carte réseau sans fil Wi-Fi :

   Suffixe DNS propre à la connexion. . . :
   Description. . . . . . . . . . . . . . : Intel(R) Dual Band Wireless-AC 8265
   Adresse physique . . . . . . . . . . . : 20-16-B9-29-84-75
   DHCP activé. . . . . . . . . . . . . . : Oui
   Configuration automatique activée. . . : Oui
   Adresse IPv6 de liaison locale. . . . .: fe80::b418:90a8:d4c4:3f44%33(préféré)
   Adresse IPv4. . . . . . . . . . . . . .: 10.33.48.12(préféré)
   Masque de sous-réseau. . . . . . . . . : 255.255.252.0
   Bail obtenu. . . . . . . . . . . . . . : 16 octobre 2023 03:17:25
   Bail expirant. . . . . . . . . . . . . : 17 octobre 2023 03:17:24
   Passerelle par défaut. . . . . . . . . : 10.33.51.254
   Serveur DHCP . . . . . . . . . . . . . : 10.33.51.254
   IAID DHCPv6 . . . . . . . . . . . : 169875129
   DUID de client DHCPv6. . . . . . . . : 00-01-00-01-2A-D2-4C-9E-8C-16-45-D4-A8-45
   Serveurs DNS. . .  . . . . . . . . . . : 10.33.10.2
                                       8.8.8.8
   NetBIOS sur Tcpip. . . . . . . . . . . : Activé
```
- adresse ip (DNS)
 Adresse IPv4. . . . . . . . . . . . . .: 192.168.56.1

🌞 Utiliser, en ligne de commande l'outil nslookup (Windows, MacOS) ou dig (GNU/Linux, MacOS) pour faire des requêtes DNS à la main

- faites un lookup 
- pour google.com

```
PS C:\Users\Usager> nslookup google.com 8.8.8.8
Serveur :   dns.google
Address:  8.8.8.8

Réponse ne faisant pas autorité :
Nom :    google.com
Addresses:  2a00:1450:4007:818::200e
          142.250.179.110
```

-pour ynov.com
```
PS C:\Users\Usager> nslookup ynov.com 8.8.8.8
Serveur :   dns.google
Address:  8.8.8.8

Réponse ne faisant pas autorité :
Nom :    ynov.com
Addresses:  2606:4700:20::681a:ae9
          2606:4700:20::ac43:4ae2
          2606:4700:20::681a:be9
          104.26.11.233
          172.67.74.226
          104.26.10.233
```
- déterminer l'adresse IP du serveur à qui vous venez d'effectuer ces requêtes

- pour google.com
Address: 142.250.179.110

- pour ynov.com
Address:  104.26.11.233
          172.67.74.226
          104.26.10.233

- faites un reverse lookup
```
PS C:\Users\Usager> nslookup 231.34.113.12  8.8.8.8
Serveur :   dns.google
Address:  8.8.8.8

*** dns.google ne parvient pas à trouver 231.34.113.12 : Non-existent domain
```
- faites un reverse lookup
```
PS C:\Users\Usager> nslookup 78.34.2.17  8.8.8.8
Serveur :   dns.google
Address:  8.8.8.8

Nom :    cable-78-34-2-17.nc.de
Address:  78.34.2.17
```
- interpréter les résultats
 pour 231.34.113.12:Non-existent domain (il n'existe aucune entrée DNS associée à cette adresse IP)
 pour  78.34.2.17 :  cable-78-34-2-17.nc.de (il existe une entrée DNS associée à cette adresse IP)


## IV. Wireshark

🌞 Utilisez le pour observer les trames qui circulent entre vos deux carte Ethernet. Mettez en évidence :

- un ping entre vous et votre mate

```
PS C:\Users\Usager> ping  192.168.1.90

Envoi d’une requête 'Ping'  192.168.1.90 avec 32 octets de données :
Réponse de 192.168.1.90 : octets=32 temps<1ms TTL=128
Réponse de 192.168.1.90 : octets=32 temps<1ms TTL=128
Réponse de 192.168.1.90 : octets=32 temps<1ms TTL=128
Réponse de 192.168.1.90 : octets=32 temps<1ms TTL=128

Statistiques Ping pour 192.168.1.90:
    Paquets : envoyés = 4, reçus = 4, perdus = 0 (perte 0%),
Durée approximative des boucles en millisecondes :
    Minimum = 0ms, Maximum = 0ms, Moyenne = 0ms
```

- un ping entre vous et la passerelle du réseau
```
PS C:\Users\Usager> ping    192.168.1.254

Envoi d’une requête 'Ping'  192.168.1.254 avec 32 octets de données :
Réponse de 192.168.1.254 : octets=32 temps=5 ms TTL=64
Réponse de 192.168.1.254 : octets=32 temps=4 ms TTL=64
Réponse de 192.168.1.254 : octets=32 temps=4 ms TTL=64
Réponse de 192.168.1.254 : octets=32 temps=9 ms TTL=64

Statistiques Ping pour 192.168.1.254:
    Paquets : envoyés = 4, reçus = 4, perdus = 0 (perte 0%),
Durée approximative des boucles en millisecondes :
    Minimum = 4ms, Maximum = 9ms, Moyenne = 5ms
```
