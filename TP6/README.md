# TP6

0. Prérequis

## I. Setup routeur
🖥️ Machine router.tp6.b1
```
PS C:\Users\Usager> ssh eunice@10.6.1.254
```

```
[eunice@router ~]$  sudo firewall-cmd --add-masquerade --permanent
[sudo] password for eunice:
success
[eunice@router ~]$ sudo firewall-cmd --reload
success
```
🖥️ Machine john.tp6.b1
```
PS C:\Users\Usager> ssh eunice@10.6.1.11
```
- testez tout de suite avec john que votre routeur fonctionne et que vous avez un accès internet
```
[eunice@john ~]$ ping www.google.com
PING www.google.com (172.217.20.164) 56(84) bytes of data.
64 bytes from par10s49-in-f4.1e100.net (172.217.20.164): icmp_seq=1 ttl=115 time=13.4 ms
64 bytes from waw02s07-in-f4.1e100.net (172.217.20.164): icmp_seq=2 ttl=115 time=14.5 ms
64 bytes from waw02s07-in-f4.1e100.net (172.217.20.164): icmp_seq=3 ttl=115 time=13.1 ms
64 bytes from par10s49-in-f4.1e100.net (172.217.20.164): icmp_seq=4 ttl=115 time=12.7 ms
64 bytes from waw02s07-in-f4.1e100.net (172.217.20.164): icmp_seq=5 ttl=115 time=14.4 ms
64 bytes from waw02s07-in-f164.1e100.net (172.217.20.164): icmp_seq=6 ttl=115 time=13.2 ms
^C
--- www.google.com ping statistics ---
6 packets transmitted, 6 received, 0% packet loss, time 5013ms
rtt min/avg/max/mdev = 12.749/13.567/14.546/0.669 ms
[eunice@john ~]$
```
## II. Serveur DNS

1. Présentation
2. Setup
🖥️ Machine dns.tp6.b1

- Installation du serveur DNS :


```
[eunice@dns ~]$ sudo dnf install -y bind bind-utils
[sudo] password for eunice:
Rocky Linux 9 - BaseOS                       13 kB/s | 4.1 kB     00:00
Rocky Linux 9 - AppStream                    13 kB/s | 4.5 kB     00:00
Rocky Linux 9 - Extras                      9.2 kB/s | 2.9 kB     00:00
Dependencies resolved.
============================================================================
 Package                      Arch   Version                Repo       Size
============================================================================
Installing:
 bind                         x86_64 32:9.16.23-11.el9_2.2  appstream 487 k
 bind-utils                   x86_64 32:9.16.23-11.el9_2.2  appstream 198 k
Installing dependencies:
 bind-dnssec-doc              noarch 32:9.16.23-11.el9_2.2  appstream  44 k
 bind-libs                    x86_64 32:9.16.23-11.el9_2.2  appstream 1.2 M
 (........)
  python3-setuptools-53.0.0-12.el9.noarch

Complete!
```

🌞 Dans le rendu, je veux

1. un cat des 3 fichiers de conf (fichier principal + deux fichiers de zone)
 
 - **fichier principal**
 ```
  [eunice@dns ~]$ sudo nano /etc/named.conf
 [eunice@dns ~]$  sudo cat /etc/named.conf
options {
    listen-on port 53 { 127.0.0.1; any; };
    listen-on-v6 port 53 { ::1; };
    directory "/var/named";
    allow-query { localhost; any; };
    allow-query-cache { localhost; any; };
    recursion yes;
};

// zone de déclaration pour tp6.b1
zone "tp6.b1" IN {
    type master;
    file "tp6.b1.db";
    allow-update { none; };
    allow-query { any; };
};

// zone de déclaration pour 1.4.10.in-addr.arpa
zone "1.4.10.in-addr.arpa" IN {
    type master;
    file "tp6.b1.rev";
    allow-update { none; };
    allow-query { any; };
};

 ```
**zone**
```
[eunice@dns ~]$ sudo nano /var/named/tp6.b1.db
[eunice@dns ~]$  sudo cat /var/named/tp6.b1.db
$TTL 86400
@ IN SOA dns.tp6.b1. admin.tp6.b1. (
    2019061800 ;Serial
    3600 ;Refresh
    1800 ;Retry
    604800 ;Expire
    86400 ;Minimum TTL
)

; Infos sur le serveur DNS lui même (NS = NameServer)
@ IN NS dns.tp6.b1.

; Enregistrements DNS pour faire correspondre des noms à des IPs
dns       IN A 10.6.1.101
john      IN A 10.6.1.11
```
**zone inverse**
```
[eunice@dns ~]$ sudo nano /var/named/tp6.b1.db
[eunice@dns ~]$ sudo cat /var/named/tp6.b1.rev
$TTL 86400
@ IN SOA dns.tp6.b1. admin.tp6.b1. (
    2019061800 ;Serial
    3600 ;Refresh
    1800 ;Retry
    604800 ;Expire
    86400 ;Minimum TTL
)

; Infos sur le serveur DNS lui même (NS = NameServer)
@ IN NS dns.tp6.b1.

; Reverse lookup
101 IN PTR dns.tp6.b1.
11 IN PTR john.tp6.b1.
```
2. Prouve que le service tourne bien
```
[eunice@dns ~]$ systemctl status named
● named.service - Berkeley Internet Name Domain (DNS)
     Loaded: loaded (/usr/lib/systemd/system/named.service; enabled; preset>
     Active: active (running) since Thu 2023-11-23 17:33:32 CET; 12min ago
   Main PID: 2084 (named)
      Tasks: 5 (limit: 4673)
     Memory: 15.3M
        CPU: 153ms
     CGroup: /system.slice/named.service
             └─2084 /usr/sbin/named -u named -c /etc/named.conf

Nov 23 17:33:32 dns named[2084]: network unreachable resolving './DNSKEY/IN>
Nov 23 17:33:32 dns named[2084]: network unreachable resolving './NS/IN': 2>
Nov 23 17:33:32 dns named[2084]: network unreachable resolving './DNSKEY/IN>
Nov 23 17:33:32 dns named[2084]: network unreachable resolving './NS/IN': 2>
Nov 23 17:33:32 dns named[2084]: zone tp6.b1/IN: loaded serial 2019061800
Nov 23 17:33:32 dns named[2084]: all zones loaded
Nov 23 17:33:32 dns systemd[1]: Started Berkeley Internet Name Domain (DNS).
Nov 23 17:33:32 dns named[2084]: running
Nov 23 17:33:32 dns named[2084]: managed-keys-zone: Initializing automatic >
Nov 23 17:33:32 dns named[2084]: resolver priming query complete
lines 1-20/20 (END)
```
3. Prouve que le service écoute bien sur un port
```
[eunice@dns ~]$ ss -tuln | grep -E ':53\s+'
udp   UNCONN 0      0          10.0.3.15:53        0.0.0.0:*
udp   UNCONN 0      0         10.6.1.101:53        0.0.0.0:*
udp   UNCONN 0      0          127.0.0.1:53        0.0.0.0:*
udp   UNCONN 0      0              [::1]:53           [::]:*
tcp   LISTEN 0      10         10.0.3.15:53        0.0.0.0:*
tcp   LISTEN 0      10        10.6.1.101:53        0.0.0.0:*
tcp   LISTEN 0      10         127.0.0.1:53        0.0.0.0:*
tcp   LISTEN 0      10             [::1]:53           [::]:*
```

🌞 Ouvrez le bon port dans le firewall
```
[eunice@dns ~]$ [eunice@dns ~]$ sudo firewall-cmd --add-port=53/tcp --permanent
success
[eunice@dns ~]$ sudo firewall-cmd --reload
success
```
3. Test
🌞 Sur la machine john.tp6.b1
```
[eunice@john ~]$ sudo cat /etc/sysconfig/network-scripts/ifcfg-enp0s3
DEVICE=enp0s3

BOOTPROTO=static
ONBOOT=yes

IPADDR=10.6.1.11
NETMASK=255.255.255.0
DNS= 192.168.1.254
```
**resoudre non de domain**
- john.tp6.b1
```
[eunice@john ~]$ dig john.tp6.b1

; <<>> DiG 9.16.23-RH <<>> john.tp6.b1
;; global options: +cmd
;; Got answer:
;; ->>HEADER<<- opcode: QUERY, status: NXDOMAIN, id: 23737
;; flags: qr rd ra; QUERY: 1, ANSWER: 0, AUTHORITY: 1, ADDITIONAL: 1

;; OPT PSEUDOSECTION:
; EDNS: version: 0, flags:; udp: 1220
; COOKIE: b65237dd14122e91a7fd068d6564f6f0427c5d2b54289504 (good)
;; QUESTION SECTION:
;john.tp6.b1.                   IN      A

;; AUTHORITY SECTION:
.                       300     IN      SOA     a.root-servers.net. nstld.verisign-grs.com. 2023112701 1800 900 604800 86400

;; Query time: 36 msec
;; SERVER: 192.168.1.254#53(192.168.1.254)
;; WHEN: Mon Nov 27 21:06:00 CET 2023
;; MSG SIZE  rcvd: 143
```
-  dns.tp6.b1
```
[eunice@john ~]$ dig dns.tp6.b1

; <<>> DiG 9.16.23-RH <<>> dns.tp6.b1
;; global options: +cmd
;; Got answer:
;; ->>HEADER<<- opcode: QUERY, status: NXDOMAIN, id: 9860
;; flags: qr rd ra; QUERY: 1, ANSWER: 0, AUTHORITY: 1, ADDITIONAL: 1

;; OPT PSEUDOSECTION:
; EDNS: version: 0, flags:; udp: 1220
; COOKIE: e2d86cf0952bc0ff1a9b49cb6564faf0cbc6f5a649b5f202 (good)
;; QUESTION SECTION:
;dns.tp6.b1.                    IN      A

;; AUTHORITY SECTION:
.                       300     IN      SOA     a.root-servers.net. nstld.verisign-grs.com. 2023112701 1800 900 604800 86400

;; Query time: 52 msec
;; SERVER: 192.168.1.254#53(192.168.1.254)
;; WHEN: Mon Nov 27 21:24:16 CET 2023
;; MSG SIZE  rcvd: 142

```
- ynov
```
[eunice@john ~]$ dig www.ynov.com

; <<>> DiG 9.16.23-RH <<>> www.ynov.com
;; global options: +cmd
;; Got answer:
;; ->>HEADER<<- opcode: QUERY, status: NOERROR, id: 38714
;; flags: qr rd ra; QUERY: 1, ANSWER: 3, AUTHORITY: 0, ADDITIONAL: 1

;; OPT PSEUDOSECTION:
; EDNS: version: 0, flags:; udp: 1220
; COOKIE: fd6cb0a41847b141227d31f96564faae20bc656a13c43fac (good)
;; QUESTION SECTION:
;www.ynov.com.                  IN      A

;; ANSWER SECTION:
www.ynov.com.           300     IN      A       104.26.10.233
www.ynov.com.           300     IN      A       172.67.74.226
www.ynov.com.           300     IN      A       104.26.11.233

;; Query time: 24 msec
;; SERVER: 192.168.1.254#53(192.168.1.254)
;; WHEN: Mon Nov 27 21:23:10 CET 2023
;; MSG SIZE  rcvd: 117

```
🌞 Sur votre PC

```
[eunice@john ~]$ dig 10.6.1.101

; <<>> DiG 9.16.23-RH <<>> 10.6.1.101
;; global options: +cmd
;; Got answer:
;; ->>HEADER<<- opcode: QUERY, status: NOERROR, id: 29150
;; flags: qr aa rd ra ad; QUERY: 1, ANSWER: 1, AUTHORITY: 0, ADDITIONAL: 1

;; OPT PSEUDOSECTION:
; EDNS: version: 0, flags:; udp: 1280
;; QUESTION SECTION:
;10.6.1.101.                    IN      A

;; ANSWER SECTION:
10.6.1.101.             0       IN      A       10.6.1.101

;; Query time: 6 msec
;; SERVER: 192.168.1.254#53(192.168.1.254)
;; WHEN: Mon Nov 27 21:21:49 CET 2023
;; MSG SIZE  rcvd: 55
```
🦈 Capture tp6_dns.pcapng
 [ceci est un lien vers un pcap](./tp6_dns.pcapng) _**tp6_dns.pcapng**_



III. Serveur DHCP




 
