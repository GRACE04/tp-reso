# TP2 : Ethernet, IP, et ARP

## I. Setup IP
🌞 Mettez en place une configuration réseau fonctionnelle entre les deux machines
```
PS C:\Users\Usager> ipconfig /all
Carte réseau sans fil Wi-Fi :

   Suffixe DNS propre à la connexion. . . : lan
   Description. . . . . . . . . . . . . . : Intel(R) Dual Band Wireless-AC 8265
   Adresse physique . . . . . . . . . . . : 20-16-B9-29-84-75
   DHCP activé. . . . . . . . . . . . . . : Oui
   Configuration automatique activée. . . : Oui
   Adresse IPv6. . . . . . . . . . . . . .: 2001:861:81:9910:efd6:e0e7:6980:91a1(préféré)
   Adresse IPv6 temporaire . . . . . . . .: 2001:861:81:9910:35a7:93bd:3a6a:d636(préféré)
   Adresse IPv6 de liaison locale. . . . .: fe80::b418:90a8:d4c4:3f44%35(préféré)
   Adresse IPv4. . . . . . . . . . . . . .: 192.168.1.90(préféré)
   Masque de sous-réseau. . . . . . . . . : 255.255.255.0
   Bail obtenu. . . . . . . . . . . . . . : 22 octobre 2023 12:23:22
   Bail expirant. . . . . . . . . . . . . : 23 octobre 2023 12:23:21
   Passerelle par défaut. . . . . . . . . : fe80::f24d:d4ff:fe26:295c%35
                                       192.168.1.254
   Serveur DHCP . . . . . . . . . . . . . : 192.168.1.254
   IAID DHCPv6 . . . . . . . . . . . : 169875129
   DUID de client DHCPv6. . . . . . . . : 00-01-00-01-2A-D2-4C-9E-8C-16-45-D4-A8-45
   Serveurs DNS. . .  . . . . . . . . . . : 2001:861:81:9910:f24d:d4ff:fe26:295c
                                       192.168.1.254
   NetBIOS sur Tcpip. . . . . . . . . . . : Activé
   Liste de recherche de suffixes DNS propres à la connexion :
                                       lan

```
- les deux IPs choisies, en précisant le masque
 **Machine 1** : Adresse IPv4. . . . . . . . . . . . . .: 192.168.1.90(préféré)
              Masque de sous-réseau. . . . . . . . . : 255.255.255.0
 **Machine 2** : Adresse IPv4. . . . . . . . . . . . . .: 192.168.1.58(préféré)
              Masque de sous-réseau. . . . . . . . . : 255.255.252.0

- l'adresse de réseau :
**Machine 1 & 2 :**  Masque de sous-réseau. . . . . . . . . : 255.255.255.0

- l'adresse de broadcast :
 **Machine 1 & 2 :** 192.168.1.254

🌞 Prouvez que la connexion est fonctionnelle entre les deux machines
**Machine 1**
```
PS C:\Users\Usager> ping 192.168.1.58

Envoi d’une requête 'Ping'  192.168.1.58 avec 32 octets de données :
Réponse de 192.168.1.58 : octets=32 temps=4 ms TTL=64
Réponse de 192.168.1.58 : octets=32 temps=5 ms TTL=64
Réponse de 192.168.1.58 : octets=32 temps=6 ms TTL=64
Réponse de 192.168.1.58 : octets=32 temps=7 ms TTL=64

Statistiques Ping pour 192.168.1.58:
    Paquets : envoyés = 4, reçus = 4, perdus = 0 (perte 0%),
Durée approximative des boucles en millisecondes :
    Minimum = 4ms, Maximum = 7ms, Moyenne = 5ms
```
**Machine 2**
```
PS C:\Users\yohan> ping 192.168.1.90

Envoi d’une requête 'Ping'  192.168.1.90 avec 32 octets de données :
Réponse de 192.168.1.90 : octets=32 temps=8 ms TTL=128
Réponse de 192.168.1.90 : octets=32 temps=48 ms TTL=128
Réponse de 192.168.1.90 : octets=32 temps=9 ms TTL=128
Réponse de 192.168.1.90 : octets=32 temps=82 ms TTL=128

Statistiques Ping pour 192.168.1.90:
    Paquets : envoyés = 4, reçus = 4, perdus = 0 (perte 0%),
Durée approximative des boucles en millisecondes :
    Minimum = 8ms, Maximum = 82ms, Moyenne = 36ms
```
🌞 Wireshark it
- déterminez, grâce à Wireshark, quel type de paquet ICMP est envoyé par ping

🦈 PCAP qui contient les paquets ICMP qui vous ont permis d'identifier les types ICMP 


## II. ARP my bro

🌞 Check the ARP table

- utilisez une commande pour afficher votre table ARP
```
PS C:\Users\Usager> arp -a
Interface : 192.168.1.90 --- 0x23
  Adresse Internet      Adresse physique      Type
  192.168.1.52          94-08-53-bc-7d-79     dynamique
  192.168.1.58          94-08-53-bc-7d-79     dynamique
  192.168.1.80          ec-6c-9a-e4-87-4b     dynamique
  192.168.1.254         f0-4d-d4-26-29-5c     dynamique
  192.168.1.255         ff-ff-ff-ff-ff-ff     statique
  224.0.0.2             01-00-5e-00-00-02     statique
  224.0.0.22            01-00-5e-00-00-16     statique
  224.0.0.251           01-00-5e-00-00-fb     statique
  224.0.0.252           01-00-5e-00-00-fc     statique
  239.255.255.250       01-00-5e-7f-ff-fa     statique
  255.255.255.255       ff-ff-ff-ff-ff-ff     statique
```
- déterminez la MAC de votre binome depuis votre table ARP

```
PS C:\Users\Usager> arp -a 192.168.1.58

Interface : 192.168.1.90 --- 0x23
  Adresse Internet      Adresse physique      Type
  192.168.1.58          94-08-53-bc-7d-79     dynamique
```
- déterminez la MAC de la gateway de votre réseau
```
PS C:\Users\Usager> arp -a 192.168.1.254

Interface : 192.168.1.90 --- 0x23
  Adresse Internet      Adresse physique      Type
  192.168.1.254         f0-4d-d4-26-29-5c     dynamique
```

🌞 Manipuler la table ARP
- utilisez une commande pour vider votre table ARP


🌞 Wireshark it

🦈 PCAP qui contient les DEUX trames ARP

### III. DHCP

🌞 Wireshark it

🦈 PCAP qui contient l'échange DORA



