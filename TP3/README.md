#TP3 RESO 

# I. ARP
  1. Echange ARP

🌞Générer des requêtes ARP
- Effectuer un ping d'une machine à l'autre

**PC1**
```
[eunice@localhost ~]$ ping 10.3.1.11
PING 10.3.1.11 (10.3.1.11) 56(84) bytes of data.
64 bytes from 10.3.1.11: icmp_seq=1 ttl=64 time=2.85 ms
64 bytes from 10.3.1.11: icmp_seq=2 ttl=64 time=4.02 ms
```
**PC2**
```
[eunice@localhost ~]$ ping 10.3.1.12
PING 10.3.1.12 (10.3.1.12) 56(84) bytes of data.
64 bytes from 10.3.1.12: icmp_seq=1 ttl=64 time=3.94 ms
64 bytes from 10.3.1.12: icmp_seq=2 ttl=64 time=3.17 ms
```
- Observer les tables ARP des deux machines
- Repérer l'adresse MAC de ```john``` dans la table ARP de ```marcel``` et vice-versa

**PC1** ```john```
```
[eunice@localhost ~]$ ip neigh show
10.3.1.1 dev enp0s3 lladdr 0a:00:27:00:00:31 REACHABLE
10.3.1.12 dev enp0s3 lladdr 08:00:27:ab:fc:ea REACHABLE
```
**PC2**  ```marcel``` 
```
[eunice@localhost ~]$ ip neigh show
10.3.1.11 dev enp0s3 lladdr 08:00:27:42:12:d8 STALE
10.3.1.1 dev enp0s3 lladdr 0a:00:27:00:00:31 REACHABLE
```
- Prouvez que l'info est correcte
1. une commande pour voir la MAC de marcel dans la table ARP de john

**PC1** ```john```

```
[eunice@localhost ~]$ ip neigh show 10.3.1.12
10.3.1.12 dev enp0s3 lladdr 08:00:27:ab:fc:ea STALE
```
**PC2**  ```marcel``` 
```
[eunice@localhost ~]$ ip neigh show 10.3.1.11
10.3.1.11 dev enp0s3 lladdr 08:00:27:42:12:d8 STALE
```
2. Une commande pour afficher la MAC de marcel, depuis marcel
```
[eunice@localhost ~]$ ip a
2: enp0s3: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc fq_codel state UP group default qlen 1000
    link/ether 08:00:27:ab:fc:ea brd ff:ff:ff:ff:ff:ff
    inet 10.3.1.12/24 brd 10.3.1.255 scope global noprefixroute enp0s3
       valid_lft forever preferred_lft forever
    inet6 fe80::a00:27ff:feab:fcea/64 scope link
       valid_lft forever preferred_lft forever

```
MAC : **08:00:27:ab:fc:ea**

2. Analyse de trames

 🌞Analyse de trames
 - utilisez la commande **tcpdump** pour réaliser une capture de 
 **PC1** ```john```

 ```
 [eunice@localhost ~]$ sudo tcpdump -i enp0s3 -c 2
[sudo] password for eunice:
dropped privs to tcpdump
tcpdump: verbose output suppressed, use -v[v]... for full protocol decode
listening on enp0s3, link-type EN10MB (Ethernet), snapshot length 262144 bytes
11:58:38.844869 IP localhost.localdomain.ssh > 10.3.1.1.49916: Flags [P.], seq 314514612:314514672, ack 3901333891, win 501, length 60
11:58:38.848889 IP localhost.localdomain.ssh > 10.3.1.1.49916: Flags [P.], seq 60:96, ack 1, win 501, length 36
2 packets captured
18 packets received by filter
0 packets dropped by kernel
 ```
 **PC2** ```marcel```

```
0 packets dropped by kernel
[eunice@localhost ~]$ sudo tcpdump -i enp0s3 -c 2
dropped privs to tcpdump
tcpdump: verbose output suppressed, use -v[v]... for full protocol decode
listening on enp0s3, link-type EN10MB (Ethernet), snapshot length 262144 bytes
11:55:00.738989 IP localhost.localdomain.ssh > 10.3.1.1.49953: Flags [P.], seq 2650058511:2650058571, ack 3609545452, win 501, length 60
11:55:00.741053 IP localhost.localdomain.ssh > 10.3.1.1.49953: Flags [P.], seq 60:96, ack 1, win 501, length 36
2 packets captured
18 packets received by filter
0 packets dropped by kernel

```
- videz vos tables ARP, sur les deux machines, puis effectuez un ping

 **PC1** ```john``` &   **PC2** ```marcel```
```
[eunice@localhost ~]$ sudo ip neigh flush all
[sudo] password for eunice:
[eunice@localhost ~]$ ls
[eunice@localhost ~]$
```
 🦈 Capture réseau tp3_arp.pcapng qui contient un ARP request et un ARP reply

```
[eunice@localhost ~]$ sudo tcpdump -i enp0s3 -c 2
[sudo] password for eunice:
dropped privs to tcpdump
tcpdump: verbose output suppressed, use -v[v]... for full protocol decode
listening on enp0s3, link-type EN10MB (Ethernet), snapshot length 262144 bytes
12:15:34.005328 IP localhost.localdomain.ssh > 10.3.1.1.49953: Flags [P.], seq 2650062863:2650062923, ack 3609548116, win 501, length 60
12:15:34.008280 IP localhost.localdomain.ssh > 10.3.1.1.49953: Flags [P.], seq 60:96, ack 1, win 501, length 36
2 packets captured
16 packets received by filter
0 packets dropped by kernel
[eunice@localhost ~]$ sudo tcpdump -i enp0s3 -c 2 -w tp3_arp.pcapng
dropped privs to tcpdump
tcpdump: listening on enp0s3, link-type EN10MB (Ethernet), snapshot length 262144 bytes
2 packets captured
13 packets received by filter
0 packets dropped by kernel
```
 🦈 [ceci est un lien vers un pcap](./tp3_arp.pcapng) _**tp3_arp.pcap**_
```
PS C:\Users\Usager> scp eunice@10.3.1.12:/home/eunice/tp3_arp.pcapng .
eunice@10.3.1.12's password:
tp3_arp.pcapng                            100%  260    86.9KB/s   00:00
```

 ## II. Routage
 

1. Mise en place du routage
