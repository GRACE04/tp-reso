# TP7

🖥️ Machine router.tp7.b1
```
[eunice@router7 ~]$ ip a
    inet 10.7.1.11/24 brd 10.7.1.255 scope global noprefixroute enp0s3
```
 **règles du pare-feu**
```
[eunice@router7 ~]$ sudo firewall-cmd --add-masquerade --permanent
[sudo] password for eunice:
success
[eunice@router7 ~]$ sudo firewall-cmd --reload
success
```

🖥️ Machine john.tp7.b1
```
[eunice@jhon7 ~]$ ip aff
    inet 10.7.1.254/24 brd 10.7.1.255 scope global noprefixroute enp0s3
      
```
**Vérification de la connectivité**
```
[eunice@jhon7 ~]$ ping -c 4 www.google.com
PING www.google.com (172.217.20.164) 56(84) bytes of data.
64 bytes from waw02s07-in-f4.1e100.net (172.217.20.164): icmp_seq=1 ttl=115 time=14.1 ms
64 bytes from waw02s07-in-f4.1e100.net (172.217.20.164): icmp_seq=2 ttl=115 time=19.7 ms
^C
--- www.google.com ping statistics ---
2 packets transmitted, 2 received, 0% packet loss, time 1003ms
rtt min/avg/max/mdev = 14.054/16.860/19.667/2.806 ms
```

## II. SSH
1. Fingerprint
- A. Explications

B. Manips

🌞 Effectuez une connexion SSH en vérifiant le fingerprint

**Premier connection**
```
PS C:\Users\Usager> ssh eunice@10.7.1.254
The authenticity of host '10.7.1.254 (10.7.1.254)' can't be established.
ED25519 key fingerprint is SHA256:PPtMVt1Ig/P2KZ30xRPr5u3GlXWD3dVjHGRgvh3F6k0.
This host key is known by the following other names/addresses:
    C:\Users\Usager/.ssh/known_hosts:4: 10.3.1.11
    C:\Users\Usager/.ssh/known_hosts:7: 10.3.1.12
    C:\Users\Usager/.ssh/known_hosts:8: 10.5.1.11
    C:\Users\Usager/.ssh/known_hosts:9: 10.5.1.254
    C:\Users\Usager/.ssh/known_hosts:10: 10.5.1.12
    C:\Users\Usager/.ssh/known_hosts:11: 10.4.1.254
    C:\Users\Usager/.ssh/known_hosts:12: 10.4.1.253
    C:\Users\Usager/.ssh/known_hosts:13: 10.4.1.12
    (4 additional names omitted)
Are you sure you want to continue connecting (yes/no/[fingerprint])? yes
Warning: Permanently added '10.7.1.254' (ED25519) to the list of known hosts.
eunice@10.7.1.254's password:
Last login: Thu Nov 23 11:14:29 2023 from 10.7.1.1
```
**le ```ssh ``` pour se connecter vers john**
```
PS C:\Users\Usager> ssh eunice@10.7.1.254
```
** l'empreinte du serveur**
```
ssh-keygen: /etc/ssh/ssh_host_ed25519_key: Permission denied
[eunice@jhon7 ~]$ sudo ssh-keygen -lf /etc/ssh/ssh_host_ed25519_key
[sudo] password for eunice:
256 SHA256:PPtMVt1Ig/P2KZ30xRPr5u3GlXWD3dVjHGRgvh3F6k0 /etc/ssh/ssh_host_ed25519_key.pub (ED25519)
```
2. Conf serveur SSH

🌞 Consulter l'état actuel
**verification**
1.  le serveur SSH tourne actuellement sur le port 22/tcp

```
[eunice@router7 ~]$ ss -tln | grep ':22'
LISTEN 0      128          0.0.0.0:22        0.0.0.0:*
LISTEN 0      128             [::]:22           [::]:*
```
2. SSH est disponible actuellement sur TOUTES les IPs de la machine
```
[eunice@router7 ~]$ ss -tln | grep sshd
```

🌞 Modifier la configuration du serveur SSH

 **Modification seveur SSH**
```
[eunice@router ~]$ sudo nano /etc/ssh/sshd_config
[eunice@router ~]$ sudo systemctl restart sshd
```

🌞 Prouvez que le changement a pris effet
```
[sudo] password for eunice:
#       $OpenBSD: sshd_config,v 1.104 2021/07/02 05:11:21 dtucker Exp $

(...)
Include /etc/ssh/sshd_config.d/*.conf

(...)
Port 25000
#AddressFamily any
ListenAddress 10.7.1.254
#ListenAddress ::

(...)

```

🌞 Effectuer une connexion SSH sur le nouveau port
 ```
 PS C:\Users\Usager> ssh eunice@10.7.1.254 -p 25000
eunice@10.7.1.254's password:
Last login: Fri Nov 24 10:34:41 2023 from 10.7.1.1
[eunice@router ~]$
 ```

3. Connexion par clé

- A. Explications


- B. Manips

🌞 Générer une paire de clés

```
PS C:\Users\Usager> ssh-keygen -t rsa -b 4096
Generating public/private rsa key pair.
Enter file in which to save the key (C:\Users\Usager/.ssh/id_rsa):
Enter passphrase (empty for no passphrase):
Enter same passphrase again:
Your identification has been saved in C:\Users\Usager/.ssh/id_rsa
Your public key has been saved in C:\Users\Usager/.ssh/id_rsa.pub
The key fingerprint is:
SHA256:6bGgvTouXEo+tOGRsjEVCCJcXWci7g8hQlEXf8whjTA usager@EUNICEDOLL
The key's randomart image is:
+---[RSA 4096]----+
|*++o.E++o+       |
|=.. o.+.B..      |
| . o o . +       |
|  o o . ..       |
| . . o. S        |
|+ * .ooo o       |
| @ *. ..o        |
|. O .  .         |
|   +oo.          |
+----[SHA256]-----+
```
➜ Consultez l'existence des clés si besoin
```
PS C:\Users\Usager> cat ~/.ssh/id_rsa.pub
(...)
PS C:\Users\Usager> cat ~/.ssh/id_rsa
(.....)
```

🌞 Déposer la clé publique sur une VM

**sur john**
```
[eunice@jhon7 ~]$ ssh-copy-id eunice@10.7.1.11
/usr/bin/ssh-copy-id: INFO: Source of key(s) to be installed: "/home/eunice/.ssh/id_rsa.pub"
The authenticity of host '10.7.1.11 (10.7.1.11)' can't be established.
ED25519 key fingerprint is SHA256:PPtMVt1Ig/P2KZ30xRPr5u3GlXWD3dVjHGRgvh3F6k0.
This key is not known by any other names
Are you sure you want to continue connecting (yes/no/[fingerprint])? yes
/usr/bin/ssh-copy-id: INFO: attempting to log in with the new key(s), to filter out any that are already installed
/usr/bin/ssh-copy-id: INFO: 1 key(s) remain to be installed -- if you are prompted now it is to install the new keys
eunice@10.7.1.11's password:
Permission denied, please try again.
eunice@10.7.1.11's password:

Number of key(s) added: 1

Now try logging into the machine, with:   "ssh 'eunice@10.7.1.11'"
and check to make sure that only the key(s) you wanted were added.

[eunice@jhon7 ~]$ ssh -i /home/eunice/.ssh/id_rsa.pub eunice@10.7.1.11
@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
@         WARNING: UNPROTECTED PRIVATE KEY FILE!          @
@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
Permissions 0644 for '/home/eunice/.ssh/id_rsa.pub' are too open.
It is required that your private key files are NOT accessible by others.
This private key will be ignored.
Load key "/home/eunice/.ssh/id_rsa.pub": bad permissions
eunice@10.7.1.11's password:
Last failed login: Fri Nov 24 11:33:21 CET 2023 from 10.7.1.1 on ssh:notty
There were 3 failed login attempts since the last successful login.
Last login: Fri Nov 24 11:32:56 2023 from 10.7.1.1

```

🌞 Connectez-vous en SSH à la machine
```
[eunice@jhon7 ~]$  ssh eunice@10.7.1.11
Last login: Fri Nov 24 21:52:43 2023 from 10.7.1.1
```

- C. Changement de fingerprint

🌞 Supprimer les clés sur la machine router.tp7.b1


🌞 Regénérez les clés sur la machine router.tp7.b1


🌞 Tentez une nouvelle connexion au serveur


## III. Web sécurisé

🌞 Montrer sur quel port est disponible le serveur web
```
[eunice@Web7 ~]$ ss -tln
State             Recv-Q            Send-Q                       Local Address:Port                         Peer Address:Port            Process
LISTEN            0                 128                                0.0.0.0:22                                0.0.0.0:*
LISTEN            0                 128                                   [::]:22                                   [::]:*
```

🌞 Générer une clé et un certificat sur web.tp7.b1

- **génération de la clé et du certificat**
```
[eunice@Web7 ~]$  openssl req -new -newkey rsa:2048 -days 365 -nodes -x509 -keyout server.key -out server.crt
.+........+.+.....+......+.........+....+...........+...+.+.....+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++*...+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++*...............+..+......+..........+.......................+......+.+...+......+......+.....+....+.........+...........+...+. (:::::)

```
- **On déplace dans un répertoire standard**
1. **la clé**
```
[eunice@Web7 ~]$ sudo mv server.key /etc/pki/tls/private/web.tp7.b1.key
[sudo] password for eunice:
```
2. **le Certificat**
```
[eunice@Web7 ~]$ sudo mv server.key /etc/pki/tls/private/web.tp7.b1.key
[sudo] password for eunice:
[eunice@Web7 ~]$ 
```
- **les permissions restrictives sur les deux fichiers**
Pour **la clé**
```
[eunice@Web7 ~]$ sudo chown eunice:eunice /etc/pki/tls/private/web.tp7.b1.key
[eunice@Web7 ~]$ sudo chmod 0400 /etc/pki/tls/private/web.tp7.b1.key
```
Pour **le certificat**
```
[eunice@Web7 ~]$ sudo chown eunice:eunice /etc/pki/tls/certs/web.tp7.b1.crt
[eunice@Web7 ~]$ sudo chmod 0444 /etc/pki/tls/certs/web.tp7.b1.crt
```
🌞 Modification de la conf de NGINX
 **instal**

 ```
 [eunice@Web7 ~]$ sudo yum install epel-release
(...)
Transaction Summary (.......)
Installed:
  epel-release-9-7.el9.noarch

Complete!
 ```
 ```
 [eunice@Web7 ~]$ sudo yum install nginx
(...)
Transaction Summary (........)
Installed:
  nginx-1:1.20.1-14.el9_2.1.x86_64
  nginx-core-1:1.20.1-14.el9_2.1.x86_64
  nginx-filesystem-1:1.20.1-14.el9_2.1.noarch
  rocky-logos-httpd-90.14-2.el9.noarch

Complete!
 ```

```
[eunice@Web7 ~]$ sudo nano /etc/nginx/conf.d/site_web_nul.conf
[eunice@Web7 ~]$ sudo cat /etc/nginx/conf.d/site_web_nul.conf
server {
    # on change la ligne listen
    listen 10.7.1.12:443 ssl;

    # et on ajoute deux nouvelles lignes
    ssl_certificate /etc/pki/tls/certs/web.tp7.b1.crt;
    ssl_certificate_key /etc/pki/tls/private/web.tp7.b1.key;

    server_name www.site_web_nul.b1;
    root /var/www/site_web_nul;
}
```
🌞 Conf firewall

```
[eunice@Web7 ~]$ sudo firewall-cmd --zone=public --add-port=443/tcp --permanent
success
[eunice@Web7 ~]$ sudo firewall-cmd --reload
success
[eunice@Web7 ~]$
```
🌞 Redémarrez NGINX

🌞 Prouvez que NGINX écoute sur le port 443/tcp

avec une commande ss


🌞 Visitez le site web en https
