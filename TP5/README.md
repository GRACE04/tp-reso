# RESO
## TP5 : TCP, UDP et services réseau

#### O.Prérequis
### I. First steps 

🌞 Déterminez, pour ces 5 applications, si c'est du TCP ou de l'UDP

- déterminez, pour chaque application :

- [IP]() et [port du serveur ]()auquel vous vous connectez
- [le port local]() que vous ouvrez pour vous connecter


**APP°1**  _maps_

**IP**
[198.102.61.110]

```
PS C:\Users\Usager> ping maps.com

Envoi d’une requête 'ping' sur maps.com [198.102.61.110] avec 32 octets de données :
Réponse de 198.102.61.110 : octets=32 temps=163 ms TTL=239
Réponse de 198.102.61.110 : octets=32 temps=182 ms TTL=239
Réponse de 198.102.61.110 : octets=32 temps=190 ms TTL=239
Réponse de 198.102.61.110 : octets=32 temps=195 ms TTL=239

Statistiques Ping pour 198.102.61.110:
    Paquets : envoyés = 4, reçus = 4, perdus = 0 (perte 0%),
Durée approximative des boucles en millisecondes :
    Minimum = 163ms, Maximum = 195ms, Moyenne = 182ms
```

**Port serveur**

  TCP

**Port local**
10.33.48.46


**APP°2** _discorde_


**IP**
[217.70.184.55]

```
PS C:\Users\Usager> ping discorde.com

Envoi d’une requête 'ping' sur discorde.com [217.70.184.55] avec 32 octets de données :
Réponse de 217.70.184.55 : octets=32 temps=20 ms TTL=55
Réponse de 217.70.184.55 : octets=32 temps=13 ms TTL=55
Réponse de 217.70.184.55 : octets=32 temps=13 ms TTL=55
Réponse de 217.70.184.55 : octets=32 temps=13 ms TTL=55

Statistiques Ping pour 217.70.184.55:
    Paquets : envoyés = 4, reçus = 4, perdus = 0 (perte 0%),
Durée approximative des boucles en millisecondes :
    Minimum = 13ms, Maximum = 20ms, Moyenne = 14ms
```

**Port serveur**

  TCP

 **Port local**
10.33.48.46


**APP°3** _youtube_

**IP**
[142.250.179.110]

```
PS C:\Users\Usager> ping youtube.com

Envoi d’une requête 'ping' sur youtube.com [142.250.179.110] avec 32 octets de données :
Réponse de 142.250.179.110 : octets=32 temps=17 ms TTL=116
Réponse de 142.250.179.110 : octets=32 temps=19 ms TTL=116
Réponse de 142.250.179.110 : octets=32 temps=19 ms TTL=116
Réponse de 142.250.179.110 : octets=32 temps=20 ms TTL=116

Statistiques Ping pour 142.250.179.110:
    Paquets : envoyés = 4, reçus = 4, perdus = 0 (perte 0%),
Durée approximative des boucles en millisecondes :
    Minimum = 17ms, Maximum = 20ms, Moyenne = 18ms

```
 **Port serveur** 

  UDP

  **Port local**
10.33.48.46

**APP°4** _figma_

**IP**
[18.155.129.115]
```
PS C:\Users\Usager> ping figma.com

Envoi d’une requête 'ping' sur figma.com [18.155.129.115] avec 32 octets de données :
Réponse de 18.155.129.115 : octets=32 temps=11 ms TTL=248
Réponse de 18.155.129.115 : octets=32 temps=14 ms TTL=248
Réponse de 18.155.129.115 : octets=32 temps=16 ms TTL=248
Réponse de 18.155.129.115 : octets=32 temps=52 ms TTL=248

Statistiques Ping pour 18.155.129.115:
    Paquets : envoyés = 4, reçus = 4, perdus = 0 (perte 0%),
Durée approximative des boucles en millisecondes :
    Minimum = 11ms, Maximum = 52ms, Moyenne = 23ms


```
  **Port serveur**

  TCP

  **Port local**
10.33.48.46

**APP°5** _gmail_

**IP**
[216.58.214.165]

```
Durée approximative des boucles en millisecondes :
    Minimum = 17ms, Maximum = 20ms, Moyenne = 18ms
PS C:\Users\Usager> ping gmail.com

Envoi d’une requête 'ping' sur gmail.com [216.58.214.165] avec 32 octets de données :
Réponse de 216.58.214.165 : octets=32 temps=17 ms TTL=116
Réponse de 216.58.214.165 : octets=32 temps=20 ms TTL=116
Réponse de 216.58.214.165 : octets=32 temps=19 ms TTL=116
Réponse de 216.58.214.165 : octets=32 temps=17 ms TTL=116

Statistiques Ping pour 216.58.214.165:
    Paquets : envoyés = 4, reçus = 4, perdus = 0 (perte 0%),
Durée approximative des boucles en millisecondes :
    Minimum = 17ms, Maximum = 20ms, Moyenne = 18ms

```
  **Port serveur**

  TCP

**Port local**
10.33.48.46

🌞 Demandez l'avis à votre OS

- **APP°1**

**_maps_**

```
PS C:\Users\Usager> netstat -an | Select-String "198.102.61.110" | Select-String "ESTABLISHED"
PS C:\Users\Usager> netstat -an

Connexions actives

  Proto  Adresse locale         Adresse distante       État
 
TCP    10.3.1.1:63382         10.3.1.12:22           ESTABLISHED
  TCP    10.3.1.1:63385         10.3.1.11:22           ESTABLISHED
  TCP    10.33.71.59:139        0.0.0.0:0              LISTENING
  TCP    10.33.71.59:63688      137.135.225.146:443    ESTABLISHED
  TCP    10.33.71.59:63690      52.112.120.9:443       ESTABLISHED
  TCP    10.33.71.59:63693      20.199.120.182:443     ESTABLISHED
  TCP    10.33.71.59:63720      20.199.120.182:443     ESTABLISHED
  TCP    10.33.71.59:63740      52.112.238.44:443      ESTABLISHED
  TCP    10.33.71.59:64063      23.192.237.212:443     CLOSE_WAIT
  TCP    10.33.71.59:64070      13.107.246.42:443      CLOSE_WAIT
  TCP    10.33.71.59:64083      8.8.4.4:443            CLOSE_WAIT
  TCP    10.33.71.59:64088      172.64.151.134:443     CLOSE_WAIT
```

- **APP°2**

**_discord_**
 ```
PS C:\Users\Usager> netstat -an | Select-String "217.70.184.55" | Select-String "ESTABLISHED"
PS C:\Users\Usager> netstat -an

Connexions actives

  Proto  Adresse locale         Adresse distante       État

   TCP    10.3.1.1:139           0.0.0.0:0              LISTENING
  TCP    10.3.1.1:63382         10.3.1.12:22           ESTABLISHED
  TCP    10.3.1.1:63385         10.3.1.11:22           ESTABLISHED
  TCP    10.33.71.59:139        0.0.0.0:0              LISTENING
  TCP    10.33.71.59:63688      137.135.225.146:443    ESTABLISHED
  TCP    10.33.71.59:63690      52.112.120.9:443       ESTABLISHED
  TCP    10.33.71.59:63693      20.199.120.182:443     ESTABLISHED
  TCP    10.33.71.59:63720      20.199.120.182:443     ESTABLISHED
  TCP    10.33.71.59:63740      52.112.238.44:443      ESTABLISHED
  TCP    10.33.71.59:64063      23.192.237.212:443     CLOSE_WAIT
  TCP    10.33.71.59:64070      13.107.246.42:443      CLOSE_WAIT
  TCP    10.33.71.59:64105      74.125.206.188:5228    ESTABLISHED
  TCP    10.33.71.59:64197      52.123.242.233:443     ESTABLISHED
  TCP    10.33.71.59:64207      2.18.132.199:80        TIME_WAIT
  TCP    10.33.71.59:64210      8.241.17.254:80        ESTABLISHED
  TCP    10.33.71.59:64211      52.97.135.50:443       ESTABLISHED

 ```
- **APP°3**

 **_youtub_**

```
PS C:\Users\Usager> netstat -an -p udp | Select-String "142.250.179.110"
PS C:\Users\Usager> netstat -an -p udp

Connexions actives

  Proto  Adresse locale         Adresse distante       État

  UDP    10.3.1.1:137           *:*
  UDP    10.3.1.1:138           *:*
  UDP    10.3.1.1:1900          *:*
  UDP    10.3.1.1:56721         *:*
  UDP    10.33.71.59:137        *:*
  UDP    10.33.71.59:138        *:*
  UDP    10.33.71.59:1900       *:*
  UDP    10.33.71.59:56723      *:*
  UDP    127.0.0.1:1900         *:*
  UDP    127.0.0.1:56701        127.0.0.1:56701
  UDP    127.0.0.1:56724        *:*
  UDP    192.168.160.1:137      *:*
  UDP    192.168.160.1:138      *:*
  UDP    192.168.160.1:1900     *:*
  UDP    192.168.160.1:56722    *:*
  UDP    192.168.210.1:137      *:*
  UDP    192.168.210.1:138      *:*
  UDP    192.168.210.1:1900     *:*
  UDP    192.168.210.1:56720    *:*
```
- **APP°4**

**_figma_**

```
S C:\Users\Usager> netstat -an -p udp | Select-String "18.155.129.115"
PS C:\Users\Usager> netstat -an -p udp

Connexions actives

  Proto  Adresse locale         Adresse distante       État

   TCP    10.3.1.1:63382         10.3.1.12:22           ESTABLISHED
  TCP    10.3.1.1:63385         10.3.1.11:22           ESTABLISHED
  TCP    10.33.71.59:139        0.0.0.0:0              LISTENING
  TCP    10.33.71.59:63688      137.135.225.146:443    ESTABLISHED
  TCP    10.33.71.59:63690      52.112.120.9:443       ESTABLISHED
  TCP    10.33.71.59:63693      20.199.120.182:443     ESTABLISHED
  TCP    10.33.71.59:63720      20.199.120.182:443     ESTABLISHED
  TCP    10.33.71.59:63740      52.112.238.44:443      ESTABLISHED
  TCP    10.33.71.59:64063      23.192.237.212:443     CLOSE_WAIT
  TCP    10.33.71.59:64070      13.107.246.42:443      CLOSE_WAIT
  TCP    10.33.71.59:64105      74.125.206.188:5228    ESTABLISHED
  TCP    10.33.71.59:64197      52.123.242.233:443     ESTABLISHED
  TCP    10.33.71.59:64213      104.46.162.226:443     ESTABLISHED
  TCP    10.33.71.59:64214      204.79.197.239:443     ESTABLISHED
  TCP    10.33.71.59:64215      52.123.128.14:443      ESTABLISHED
  TCP    10.33.71.59:64216      52.182.141.63:443      ESTABLISHED
```
-**APP°5**

**_gmail_**

```
PS C:\Users\Usager> netstat -an | Select-String "216.58.214.165" | Select-String "ESTABLISHED"
PS C:\Users\Usager> netstat -an

Connexions actives

  Proto  Adresse locale         Adresse distante       État
    TCP    10.3.1.1:63382         10.3.1.12:22           ESTABLISHED
  TCP    10.3.1.1:63385         10.3.1.11:22           ESTABLISHED
  TCP    10.33.71.59:139        0.0.0.0:0              LISTENING
  TCP    10.33.71.59:63688      137.135.225.146:443    ESTABLISHED
  TCP    10.33.71.59:63690      52.112.120.9:443       ESTABLISHED
  TCP    10.33.71.59:63693      20.199.120.182:443     ESTABLISHED
  TCP    10.33.71.59:63720      20.199.120.182:443     ESTABLISHED
  TCP    10.33.71.59:63740      52.112.238.44:443      ESTABLISHED
  TCP    10.33.71.59:64063      23.192.237.212:443     CLOSE_WAIT
  TCP    10.33.71.59:64070      13.107.246.42:443      CLOSE_WAIT
  TCP    10.33.71.59:64105      74.125.206.188:5228    ESTABLISHED
  TCP    10.33.71.59:64197      52.123.242.233:443     ESTABLISHED
  TCP    10.33.71.59:64213      104.46.162.226:443     TIME_WAIT
  TCP    10.33.71.59:64216      52.182.141.63:443      TIME_WAIT
```

- 🦈🦈🦈🦈🦈 Bah ouais, 5 captures Wireshark à l'appui évidemment. Ce sera tp5_service_1.pcapng jusqu'à tp5_service_5.pcapng.


🦈 tp5_service_1.pcapng **_maps_**
   [ceci est un lien vers un pcap](./tp5_service_1.pcapng) _**tp5_service_1.pcapng**_


🦈 tp5_service_2.pcapng **_discord_**
   [ceci est un lien vers un pcap](./tp5_service_2.pcapng) _**tp5_service_2.pcapng**_


🦈 tp5_service_3.pcapng **_youtube_**
   [ceci est un lien vers un pcap](./tp5_service_3.pcapng) _**tp5_service_3.pcapng**_


🦈 tp5_service_4.pcapng **_figma_**
   [ceci est un lien vers un pcap](./tp5_service_4.pcapng) _**tp5_service_4.pcapng**_


🦈 tp5_service_5.pcapng **_gmail_**
   [ceci est un lien vers un pcap](./tp5_service_5.pcapng) _**tp5_service_5.pcapng**_


### II. Setup Virtuel
### 1. SSH

🖥️ Machine node1.tp5.b1

```
$ ssh eunice@10.5.1.11
```

🌞 Examinez le trafic dans Wireshark

- déterminez si SSH utilise TCP ou UDP


**TCP**

🌞 Demandez aux OS
- repérez, avec une commande adaptée (netstat ou ss), la connexion SSH depuis votre machine

 **PC**
```
PS C:\Users\Usager> netstat -a -n -b
TCP    10.5.1.1:64399         10.5.1.11:22           ESTABLISHED
 [ssh.exe]
```
 **VM**
```
[eunice@node1 ~]$ sudo ss -an | grep ":22"
tcp   LISTEN 0      128                                       0.0.0.0:22               0.0.0.0:*
tcp   ESTAB  0      0                                       10.5.1.11:22              10.5.1.1:64399
tcp   LISTEN 0      128                                          [::]:22                  [::]:*

```
🦈 tp5_3_way.pcapng : une capture clean avec le 3-way handshake, un peu de trafic au milieu et une fin de connexion
  
  [ceci est un lien vers un pcap](./tp5_3_way.pcapng) _**tp5_3_way.pcapng**_
 
####  2. Routage

🖥️ Machine router.tp5.b1
```
PS C:\Users\Usager> ssh eunice@10.5.1.254
```
🌞 Prouvez que

- node1.tp5.b1 a un accès internet
```
[eunice@node1 ~]$ ping -c 4 www.google.com
PING www.google.com (142.250.201.36) 56(84) bytes of data.
64 bytes from mrs08s20-in-f4.1e100.net (142.250.201.36): icmp_seq=1 ttl=56 time=15.2 ms
64 bytes from mrs08s20-in-f4.1e100.net (142.250.201.36): icmp_seq=2 ttl=56 time=17.9 ms
64 bytes from mrs08s20-in-f4.1e100.net (142.250.201.36): icmp_seq=3 ttl=56 time=15.9 ms
64 bytes from mrs08s20-in-f4.1e100.net (142.250.201.36): icmp_seq=4 ttl=56 time=16.6 ms

--- www.google.com ping statistics ---
4 packets transmitted, 4 received, 0% packet loss, time 3007ms
rtt min/avg/max/mdev = 15.213/16.421/17.889/0.987 ms

```
- node1.tp5.b1 peut résoudre des noms de domaine publics (comme www.ynov.com)

```
[eunice@node1 ~]$ dig www.ynov.com

; <<>> DiG 9.16.23-RH <<>> www.ynov.com
;; global options: +cmd
;; Got answer:
;; ->>HEADER<<- opcode: QUERY, status: NOERROR, id: 18482
;; flags: qr rd ra; QUERY: 1, ANSWER: 3, AUTHORITY: 0, ADDITIONAL: 1

;; OPT PSEUDOSECTION:
; EDNS: version: 0, flags:; udp: 1280
;; QUESTION SECTION:
;www.ynov.com.                  IN      A

;; ANSWER SECTION:
www.ynov.com.           300     IN      A       172.67.74.226
www.ynov.com.           300     IN      A       104.26.10.233
www.ynov.com.           300     IN      A       104.26.11.233

;; Query time: 25 msec
;; SERVER: 192.168.1.254#53(192.168.1.254)
;; WHEN: Fri Nov 10 16:47:42 CET 2023
;; MSG SIZE  rcvd: 89
```
#### 3. Serveur Web


🖥️ Machine web.tp5.b1
```
PS C:\Users\Usager> ssh eunice@10.5.1.12
eunice@10.5.1.12's password:
Last login: Fri Nov 10 18:52:52 2023 from 10.5.1.1
```
🌞 Installez le paquet nginx

- avec une commande dnf install

```
[eunice@web ~]$ sudo dnf install nginx
[sudo] password for eunice:
Rocky Linux 9 - BaseOS                      7.2 kB/s | 4.1 kB     00:00
Rocky Linux 9 - AppStream                    14 kB/s | 4.5 kB     00:00
Rocky Linux 9 - Extras                      8.8 kB/s | 2.9 kB     00:00
Dependencies resolved.
============================================================================
 Package              Arch      Version                  Repository    Size
============================================================================
Installing:
 nginx                x86_64    1:1.20.1-14.el9_2.1      appstream     36 k
Installing dependencies:
 nginx-core           x86_64    1:1.20.1-14.el9_2.1      appstream    565 k
 nginx-filesystem     noarch    1:1.20.1-14.el9_2.1      appstream    8.5 k
 rocky-logos-httpd    noarch    90.14-1.el9              appstream     24 k

Transaction Summary
============================================================================
Install  4 Packages

Total download size: 634 k
Installed size: 1.8 M
Is this ok [y/N]: yes
Downloading Packages:
(1/4): nginx-filesystem-1.20.1-14.el9_2.1.n  86 kB/s | 8.5 kB     00:00
(2/4): rocky-logos-httpd-90.14-1.el9.noarch 200 kB/s |  24 kB     00:00
(3/4): nginx-1.20.1-14.el9_2.1.x86_64.rpm   264 kB/s |  36 kB     00:00
(4/4): nginx-core-1.20.1-14.el9_2.1.x86_64. 1.7 MB/s | 565 kB     00:00
----------------------------------------------------------------------------
Total                                       909 kB/s | 634 kB     00:00
Running transaction check
Transaction check succeeded.
Running transaction test
Transaction test succeeded.
Running transaction
  Preparing        :                                                    1/1
  Running scriptlet: nginx-filesystem-1:1.20.1-14.el9_2.1.noarch        1/4
  Installing       : nginx-filesystem-1:1.20.1-14.el9_2.1.noarch        1/4
  Installing       : nginx-core-1:1.20.1-14.el9_2.1.x86_64              2/4
  Installing       : rocky-logos-httpd-90.14-1.el9.noarch               3/4
  Installing       : nginx-1:1.20.1-14.el9_2.1.x86_64                   4/4
  Running scriptlet: nginx-1:1.20.1-14.el9_2.1.x86_64                   4/4
  Verifying        : rocky-logos-httpd-90.14-1.el9.noarch               1/4
  Verifying        : nginx-filesystem-1:1.20.1-14.el9_2.1.noarch        2/4
  Verifying        : nginx-1:1.20.1-14.el9_2.1.x86_64                   3/4
  Verifying        : nginx-core-1:1.20.1-14.el9_2.1.x86_64              4/4

Installed:
  nginx-1:1.20.1-14.el9_2.1.x86_64
  nginx-core-1:1.20.1-14.el9_2.1.x86_64
  nginx-filesystem-1:1.20.1-14.el9_2.1.noarch
  rocky-logos-httpd-90.14-1.el9.noarch

Complete!

```
🌞 Créer le site web

- ce dossier c'est /var/www/

```
[eunice@web /]$ sudo mkdir -p /var/www/
```
- créer donc un dossier /var/www/site_web_nul/
```
[eunice@web /]$ cd /var/www/
[eunice@web www]$ sudo mkdir /var/www/site_web_nul/
```
- créer un fichier /var/www/site_web_nul/index.html qui contient un code HTML simpliste de votre choix
```
[eunice@web ~]$ sudo touch /var/www/site_web_nul/index.html
[eunice@web ~]$ ls
site_web_nul
[eunice@web ~]$ cd site_web_nul
[eunice@web site_web_nul]$ ls
index.html
[eunice@web site_web_nul]$ sudo nano index.html
```

🌞 Donner les bonnes permissions

```
[eunice@web /]$ sudo chown -R nginx:nginx /var/www/site_web_nul
[sudo] password for eunice:
```

🌞 Créer un fichier de configuration NGINX pour notre site web

```
[eunice@web /]$ sudo nano /etc/nginx/conf.d/site_web_nul.conf
```

🌞 Démarrer le serveur web !

```
[eunice@web /]$ sudo systemctl start nginx
[sudo] password for eunice:
[eunice@webt /]$
```
- systemctl status nginx pour vérifier
```
[eunice@web /]$ systemctl status nginx
● nginx.service - The nginx HTTP and reverse proxy server
     Loaded: loaded (/usr/lib/systemd/system/nginx.service; enabled; preset>
     Active: active (running) since Fri 2023-11-10 21:15:03 CET; 6h ago
    Process: 1790 ExecStartPre=/usr/bin/rm -f /run/nginx.pid (code=exited, >
    Process: 1791 ExecStartPre=/usr/sbin/nginx -t (code=exited, status=0/SU>
    Process: 1792 ExecStart=/usr/sbin/nginx (code=exited, status=0/SUCCESS)
   Main PID: 1793 (nginx)
      Tasks: 2 (limit: 4673)
     Memory: 1.9M
        CPU: 42ms
     CGroup: /system.slice/nginx.service
             ├─1793 "nginx: master process /usr/sbin/nginx"
             └─1794 "nginx: worker process"

Nov 10 21:15:03 web.localdomain systemd[1]: Starting The nginx HTTP a>
Nov 10 21:15:03 web.localdomain nginx[1791]: nginx: the configuration>
Nov 10 21:15:03 web.localdomain nginx[1791]: nginx: configuration fil>
Nov 10 21:15:03 web.localdomain systemd[1]: Started The nginx HTTP an>
lines 1-18/18 (END)
```
🌞 Ouvrir le port firewall
```
[eunice@web /]$ sudo firewall-cmd --add-port=80/tcp --permanent
sudo firewall-cmd --reload
[sudo] password for eunice:
success
success
```

🌞 Visitez le serveur web !
- utilisez la commande curl depuis node1.tp5.b1 pour visiter le site web
```
[eunice@node1 ~]$ hostname -I
10.5.1.11  10.0.3.15
```
```
[eunice@node1 ~]$ curl http://10.5.1.12
<!doctype html>
<html>
  <head>
    <meta charset='utf-8'>
    <meta name='viewport' content='width=device-width, initial-scale=1'>
    <title>HTTP Server Test Page powered by: Rocky Linux</title>
    <style type="text/css">
      /*<![CDATA[*/

      html {
        height: 100%;
        width: 100%;
      }
        body {
(.........)
```

🌞 Visualiser le port en écoute
- avec une commande ss dans le terminal depuis web.tp5.b1
```
[eunice@web ~]$ hostname -I
10.5.1.12  10.0.3.15
```

```
[eunice@web ~]$ ss -tln | grep ':80'
LISTEN 0      511          0.0.0.0:80        0.0.0.0:*
LISTEN 0      511             [::]:80           [::]:*
```
🌞 Analyse trafic

- 🦈 tp5_web.pcapng avec le 3-way handshake, la page HTML retournée, et une fin de connexion propre si vous en avez une :

  🦈 [ceci est un lien vers un pcap](./tp5_web.pcapng) _**tp5_web.pcapng**_




