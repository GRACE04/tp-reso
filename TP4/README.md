# TP4 : DHCP

## I. DHCP Client


🌞 Déterminer
  - l'adresse du serveur DHCP
  ``` 
  PS C:\Users\Usager> ipconfig /all
  Carte réseau sans fil Wi-Fi :

   Suffixe DNS propre à la connexion. . . :
   Description. . . . . . . . . . . . . . : Intel(R) Dual Band Wireless-AC 8265
   Adresse physique . . . . . . . . . . . : 20-16-B9-29-84-75
   DHCP activé. . . . . . . . . . . . . . : Oui
   Configuration automatique activée. . . : Oui
   Adresse IPv6 de liaison locale. . . . .: fe80::b418:90a8:d4c4:3f44%31(préféré)
   Adresse IPv4. . . . . . . . . . . . . .: 10.33.71.59(préféré)
   Masque de sous-réseau. . . . . . . . . : 255.255.240.0
   Bail obtenu. . . . . . . . . . . . . . : 27 octobre 2023 03:52:58
   Bail expirant. . . . . . . . . . . . . : 28 octobre 2023 03:11:41
   Passerelle par défaut. . . . . . . . . : 10.33.79.254
   Serveur DHCP . . . . . . . . . . . . . : 10.33.79.254
   IAID DHCPv6 . . . . . . . . . . . : 169875129
   DUID de client DHCPv6. . . . . . . . : 00-01-00-01-2A-D2-4C-9E-8C-16-45-D4-A8-45
   Serveurs DNS. . .  . . . . . . . . . . : 8.8.8.8
                                       1.1.1.1
   NetBIOS sur Tcpip. . . . . . . . . . . : Activé

  ```
  Serveur DHCP : ``10.33.79.254``

 - l'heure exacte à laquelle vous avez obtenu votre bail DHCP
  Bail obtenu : 27 octobre 2023 ``03:52:58``
 - l'heure exacte à laquelle il va expirer
  Bail expirant: 28 octobre 2023 ``03:11:41``

🌞 Capturer un échange DHCP
- forcer votre OS à refaire un échange DHCP complet
```
PS C:\Users\Usager> ipconfig /release
Carte réseau sans fil Wi-Fi :

   Suffixe DNS propre à la connexion. . . :
   Adresse IPv6 de liaison locale. . . . .: fe80::b418:90a8:d4c4:3f44%31
   Passerelle par défaut. . . . . . . . . :
```
```
PS C:\Users\Usager> ipconfig /renew
Carte réseau sans fil Wi-Fi :

   Suffixe DNS propre à la connexion. . . :
   Adresse IPv6 de liaison locale. . . . .: fe80::b418:90a8:d4c4:3f44%31
   Adresse IPv4. . . . . . . . . . . . . .: 10.33.71.59
   Masque de sous-réseau. . . . . . . . . : 255.255.240.0
   Passerelle par défaut. . . . . . . . . : 10.33.79.254
```
 - utiliser Wireshark pour capturer les 4 trames DHCP
  [ceci est un lien vers un pcap](./tp4_dhcp_client.pcapng) _**tp4_dhcp_client.pcapng**_

  🌞 Analyser la capture Wireshark
 - parmi ces 4 trames, laquelle contient les informations proposées au client ?
  C'est le Offer 
  [1425 74.977287   192.168.1.1   192.168.1.23  DHCP     381 DHCP ``Offer`` - Transaction ID 0xc08ace9b]
  
  🦈 tp4_dhcp_client.pcapng

   [ceci est un lien vers un pcap](./CapOffer.pcapng) _**CapOffer.pcapng**_

    ## II. Serveur DHCP

   #### 1. Topologie
   
2. Tableau d'adressage

- router.tp4.b1
```
[eunice@router ~]$ ip a
2: enp0s3: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc fq_codel state UP group default qlen 1000
    link/ether 08:00:27:af:29:4f brd ff:ff:ff:ff:ff:ff
    inet 10.4.1.254/24 brd 10.4.1.255 scope global noprefixroute enp0s3
       valid_lft forever preferred_lft forever
    inet6 fe80::a00:27ff:feaf:294f/64 scope link
       valid_lft forever preferred_lft forever
```
- dhcp.tp4.b1
```
[eunice@dhcp ~]$ ip a
2: enp0s3: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc fq_codel state UP group default qlen 1000
    link/ether 08:00:27:c8:84:99 brd ff:ff:ff:ff:ff:ff
    inet 10.4.1.253/24 brd 10.4.1.255 scope global noprefixroute enp0s3
       valid_lft forever preferred_lft forever
    inet6 fe80::a00:27ff:fec8:8499/64 scope link
       valid_lft forever preferred_lft forever

```


- node1.tp4.b1
N/A


- node2.tp4.b1
```
[eunice@node2 ~]$ ip a
2: enp0s3: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc fq_codel state UP group default qlen 1000
    link/ether 08:00:27:b4:ce:d2 brd ff:ff:ff:ff:ff:ff
    inet 10.4.1.12/24 brd 10.4.1.255 scope global noprefixroute enp0s3
       valid_lft forever preferred_lft forever
    inet6 fe80::a00:27ff:feb4:ced2/64 scope link
       valid_lft forever preferred_lft forever

```
3. Setup topologie

🌞 Preuve de mise en place

- depuis dhcp.tp4.b1, envoyer un ping vers un nom de domaine public (pas une IP)


```
[eunice@dhcp ~]$ ping ynov.com
PING ynov.com (104.26.10.233) 56(84) bytes of data.
64 bytes from 104.26.10.233 (104.26.10.233): icmp_seq=1 ttl=55 time=17.1 ms
64 bytes from 104.26.10.233 (104.26.10.233): icmp_seq=2 ttl=55 time=17.8 ms
64 bytes from 104.26.10.233 (104.26.10.233): icmp_seq=3 ttl=55 time=20.0 ms
64 bytes from 104.26.10.233 (104.26.10.233): icmp_seq=4 ttl=55 time=20.8 ms
64 bytes from 104.26.10.233 (104.26.10.233): icmp_seq=5 ttl=55 time=17.2 ms
64 bytes from 104.26.10.233 (104.26.10.233): icmp_seq=6 ttl=55 time=17.1 ms
^C64 bytes from 104.26.10.233: icmp_seq=7 ttl=55 time=17.1 ms

--- ynov.com ping statistics ---
7 packets transmitted, 7 received, 0% packet loss, time 6016ms
rtt min/avg/max/mdev = 17.108/18.169/20.789/1.444 ms
```
- depuis node2.tp4.b1, envoyer un ping vers un nom de domaine public (pas une IP)

```
[eunice@node2 ~]$ ping ynov.com
PING ynov.com (172.67.74.226) 56(84) bytes of data.
64 bytes from 172.67.74.226 (172.67.74.226): icmp_seq=1 ttl=55 time=14.3 ms
64 bytes from 172.67.74.226 (172.67.74.226): icmp_seq=2 ttl=55 time=16.4 ms
64 bytes from 172.67.74.226 (172.67.74.226): icmp_seq=3 ttl=55 time=17.5 ms
64 bytes from 172.67.74.226 (172.67.74.226): icmp_seq=4 ttl=55 time=17.6 ms
64 bytes from 172.67.74.226 (172.67.74.226): icmp_seq=5 ttl=55 time=15.6 ms
^X64 bytes from 172.67.74.226 (172.67.74.226): icmp_seq=6 ttl=55 time=17.7 ms
64 bytes from 172.67.74.226 (172.67.74.226): icmp_seq=7 ttl=55 time=16.2 ms
64 bytes from 172.67.74.226 (172.67.74.226): icmp_seq=8 ttl=55 time=16.1 ms

--- ynov.com ping statistics ---
8 packets transmitted, 8 received, 0% packet loss, time 7018ms
rtt min/avg/max/mdev = 14.286/16.431/17.724/1.100 ms

```

- depuis node2.tp4.b1, un traceroute vers une IP publique pour montrer que vos paquets à destination d'internet passent bien par le router.tp4.b1

```
[eunice@node2 ~]$ sudo dnf install traceroute
(.............)
Installed:
  traceroute-3:2.1.0-16.el9.x86_64

Complete!

```
- 
```
[eunice@node2 ~]$ sudo tracepath 8.8.8.8
 1?: [LOCALHOST]                      pmtu 1500
 1:  _gateway                                              3.843ms
 1:  _gateway                                              1.428ms
 2:  _gateway                                             11.752ms reached
     Resume: pmtu 1500 hops 2 back 1
```
### 4. Serveur DHCP

🌞 Rendu
- toutes les commandes tapées pour monter votre serveur DHCP sur dhcp.tp4.b1


1. la commande d'installation 

```
[eunice@dhcp ~]$ sudo dnf install dhcp-server
```
2. echerche des paquets liés au terme "dhcp" 
```
[eunice@dhcp ~]$ sudo dnf search dhcp
```



### 5. Client DHCP

🌞 Test !


🌞 Prouvez que



🌞 Bail DHCP serveur


### 6. Options DHCP



🌞 Nouvelle conf !



🌞 Test !


🌞 Capture Wireshark



🦈 tp4_dhcp_server.pcapng
